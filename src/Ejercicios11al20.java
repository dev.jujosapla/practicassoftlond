import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;


public class Ejercicios11al20
{
    Scanner scanner = new Scanner(System.in);
    public void Ejercicio11()
    {
        System.out.println("Ejercicio 11");
        System.out.println("Ingrese longitud a: ");
        int longitudA = scanner.nextInt();
        System.out.println("Ingrese longitud b: ");
        int longitudB = scanner.nextInt();
        System.out.println("Ingrese longitud c: ");
        int longitudC = scanner.nextInt();
        int resultado = 0;

        resultado = (longitudA+longitudB+longitudC)/2;

        System.out.println("El area del trinagulo utilizando al formula de Heron es: " + resultado);
    }
    public void Ejercicio12()
    {
        System.out.println("Ejercicio 12");
        System.out.println("Ingrese un número positivo mayor que 0 para saber si es primo o no");
        int numero = scanner.nextInt();
        boolean esPrimo = true;

        if(numero > 1)
        {
            for(int i = 2; i < numero; i++)
            {
                if( numero % i == 0)
                {
                    esPrimo = false;
                    break;
                }
            }
            if (esPrimo)
            {
                System.out.println("El número " + numero + " es primo");
            }
            else
            {
                System.out.println("El número " + numero + " no es primo");
            }
        }
        else if (numero == 1)
        {
            System.out.println("El número 1 no es primo");
        }
        else
        {
            System.out.println("Ingrese un valor positivo mayor que 0");
        }
    }

    public void Ejercicio13()
    {
        System.out.println("Ejercicio 13");
        System.out.println("Ingrese el número con decimales que desea redondear");
        double numero = scanner.nextDouble();
        double redondear = (double)Math.round(numero*100d)/100;

        System.out.println("El número " + numero + " redondeado a 2 decimales es: " + redondear);


    }
    public void Ejercicio14()
    {
        System.out.println("Ejercicio 14");
        System.out.println("Ingrese el número para comprobar si es perfecto");
        int numero = scanner.nextInt();
        int[] divisores = new int[numero];
        int suma = 0;

        for (int i = 1; i < numero; i++)
        {
            if (numero % i  == 0)
            {
                suma = suma + i;
            }
        }

        if (suma == numero)
        {
            System.out.println("El número " + numero + " es perfecto");
        }
        else
        {
            System.out.println("El número " + numero + " no es perfecto");
        }
    }
    public void Ejercicio15()
    {
        System.out.println("Ejercicio 15");
        System.out.println("Inserte número a comprobar");
        int numero = scanner.nextInt();

        int resto;
        int falta = numero;
        int numInvertido = 0;

        while (falta > 0)
        {
            resto = falta % 10;
            falta = falta / 10;
            numInvertido = numInvertido * 10 + resto;
        }
        if(numInvertido == numero)
        {
            System.out.println( numero + " Es un número capicúa");
        }
        else
        {
            System.out.println( numero + " No es un número capicía");
        }

    }

    public void Ejercicio16()
    {
        System.out.println("Ejercicio 16");
        System.out.println("Ingrese hasta que número quiere que se ejecute la serie fibonacci");
        int numero = scanner.nextInt();
        System.out.println("La seríe fibonacci es: ");
        int numero1 = 0;
        int numero2 = 1;
        int resultado = 1;


        for( int i = 1; i <= numero; i++ )
        {
            System.out.println(resultado);

            resultado = numero1 + numero2;

            numero1 = numero2;
            numero2 = resultado;
        }
    }

    public void Ejercicio17()
    {
        System.out.println("Ejercicio 17");
        System.out.println("Ingrese el limite inferior del rango a calcular los números primos");
        int numero1 = scanner.nextInt();
        System.out.println("Ingrese el limite superior del rango a calcular los números primos");
        int numero2 = scanner.nextInt();

        int numeroMenor = numero1;
        int numeroMayor = numero2;

        if(numero1 > numero2)
        {
           numeroMenor = numero2;
           numeroMayor = numero1;
        }
        System.out.println("Los siguientes números del rango especificado son primos :");
        for (int i = numeroMenor; i <= numeroMayor; i++)
        {
            boolean esPrimo = true;

            if(i == 1)
            {
                i++;
            }
            for (int j = 2; j < i; j++)
            {
                if (i % j == 0) {
                    esPrimo = false;
                    break;
                }
            }
            if (esPrimo)
            {
                System.out.println(i);
            }
        }
    }

    public void Ejercicio18()
    {
        System.out.println("Ejercicio 18");
        int tamaño = 8;
        String indice = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        String contraseña = "";


        for(int i = 0; i < tamaño; i++)
        {
            int indiceAleatorio = numeroAleatorio(0, indice.length() - 1);
            char caracterAleatorio = indice.charAt(indiceAleatorio);
            contraseña += caracterAleatorio;
        }
        System.out.println(contraseña);
    }
    public static int numeroAleatorio(int minimo, int maximo) {

        return ThreadLocalRandom.current().nextInt(minimo, maximo + 1);
    }

    public void Ejercicio19()
    {
        System.out.println("Ejercicio 19");
        System.out.println("Ingrese su nombre");
        String nombre = scanner.nextLine();

        System.out.println("Su nombre escrito en minúsculas es: " + nombre.toLowerCase());
        System.out.println("Su nombre escrito en mayúsculas es: " + nombre.toUpperCase());
    }

    public void Ejercicio20()
    {
        System.out.println("Ejercicio 20");
        System.out.println("Ingrese texto a invertir");
        String texto = scanner.nextLine();
        String invertida = "";

        for (int i = texto.length() - 1; i >= 0; i--) {

            invertida += texto.charAt(i);
        }
        System.out.println("Cadena invertida: " + invertida);
    }
    public void Ejercicio21()
    {
        System.out.println("Ejercicio 21");
        System.out.println("Ingrese texto");
        String texto = scanner.nextLine();
        System.out.println("Ingrese letra a buscar");
        String letra = scanner.nextLine();
        int contador = 0;

        for (int i = 0 ; i < texto.length(); i++)
        {
             if(String.valueOf(texto.charAt(i)).equals(letra))
             {
                 contador++;
             }
        }
        System.out.println("El texto tiene " + contador + " a");
    }

}

