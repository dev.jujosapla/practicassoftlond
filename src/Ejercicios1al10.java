import java.util.Scanner;
public class Ejercicios1al10
{
    Scanner scanner = new Scanner(System.in);

    Ejercicios1al10(){}
    public void Ejercicio1()
    {
        System.out.println("Ejercicio 1");

        float resultado;
        System.out.println("Ingrese primer valor");
        float numero1 = scanner.nextFloat();

        System.out.println("Ingrese segundo valor");
        float numero2 = scanner.nextFloat();

        resultado = numero1 + numero2;
        System.out.println("El resultado de la suma es: " + resultado);

        resultado = numero1 - numero2;
        System.out.println("El resultado de la resta es: " + resultado);

        resultado = numero1 * numero2;
        System.out.println("El resultado de la multiplicación es: " + resultado);

        resultado = numero1 / numero2;
        System.out.println("El resultado de la división es: " + resultado);
    }

    public void Ejercicio2()
    {
        System.out.println("Ejercicio 2");

        System.out.println("Ingrese valor");
        int numero = scanner.nextInt();

        if (numero%2 == 0)
        {
            System.out.println("El número ingresado es par");
        }
        else
        {
            System.out.println("El número ingresado es impar");
        }
    }

    public void Ejercicio3()
    {
        System.out.println("Ejercicio 3");

        double pi = 3.1416;
        System.out.println("Ingrese valor del radio");
        double radio = scanner.nextDouble();
        double radioAlCuadrado = Math.pow(radio, 2);

        double perimetro = 2 * pi * radio;
        System.out.println("El valor del perimetro es: " + perimetro);

        double area = pi * radioAlCuadrado;
        System.out.println("El valor del Area es: " + area);

    }

    public void Ejercicio4()
    {
        System.out.println("Ejercicio 4");

        System.out.println("Ingrese su edad");
        int edad = scanner.nextInt();

        if(edad < 18)
        {
            System.out.println("Eres menor de edad");
        }
        else
        {
            System.out.println("Eres mayor de edad");
        }
    }

    public void Ejercicio5()
    {
        System.out.println("Ejercicio 5");

        System.out.println("Ingrese primer valor");
        int numero1 = scanner.nextInt();

        System.out.println("Ingrese segundo valor");
        int numero2 = scanner.nextInt();

        if(numero1 < numero2)
        {
            System.out.println("El numero mayor es: " + numero2);
        }
        else
        {
            System.out.println("El numero mayor es: " + numero1);
        }
    }

    public void Ejercicio6()
    {
        System.out.println("Ejercicio 6");
        System.out.println("Ingrese un número negativo o positivo");
        int numero = scanner.nextInt();

        if (numero < 0)
        {
            System.out.println("El numero " + numero + " es negativo");
        }
        else if(numero == 0)
        {
            System.out.println("El numero es 0");
        }
        else
        {
            System.out.println("El numero " + numero + " es positivo");
        }
    }

    public void Ejercicio7()
    {
        System.out.println("Ejercicio 7");
        int resultado;
        System.out.println("Ingrese un número entero positivo");
        int numero = scanner.nextInt();

        for(int i = 1; i <= 10; i++)
        {
            resultado = numero * i;
            System.out.println(numero + " * " + i + " = " + resultado);
        }
    }

    public void Ejercicio8()
    {
        System.out.println("Ejercicio 8");
        int numero = (int)(Math.random()*100+1);

        // Si se desea conocer el número elegido por el sistema descomentar la linea de abajo
        //System.out.println(numero);

        int numeroUsuario = 0;
        while(numero != numeroUsuario)
        {
            System.out.println("Inserte el número que usted cree que el sistema eligió");
            numeroUsuario = scanner.nextInt();
            if (numero != numeroUsuario)
            {
                System.out.println("El número " + numeroUsuario + " es incorrecto");
            }
            else
            {
                System.out.println("El número " + numeroUsuario + " es correcto");
            }
        }
    }

    public void Ejercicio9()
    {
        System.out.println("Ejercicio 9");
        System.out.println("Ingrese el número el cual desea conocer su valor factorial");
        int numero = scanner.nextInt();
        int resultado = 1;

        for( int i = numero; i > 0; i-- )
        {
            resultado = resultado * i;
        }
        System.out.println(resultado);
    }

    public void Ejercicio10()
    {
        System.out.println("Ejercicio 10");
        System.out.println("La seríe fibonacci es: ");

        int numero1 = 0;
        int numero2 = 1;
        int resultado = 1;


        for( int i = 1; i <= 20; i++ )
        {
            System.out.println(resultado);

            resultado = numero1 + numero2;

            numero1 = numero2;
            numero2 = resultado;
        }
    }

}
